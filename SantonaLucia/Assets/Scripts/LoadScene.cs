﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadScene : MonoBehaviour {
    public string sceneName;
        public float waitTime;
	// Use this for initialization
	void Start () {
        StartCoroutine(GoOn());
	}

    IEnumerator GoOn()
    {
        yield return new WaitForSeconds(waitTime);
        UnityEngine.SceneManagement.SceneManager.LoadScene(sceneName);
    }


    // Update is called once per frame
    void Update () {
		
	}
}
