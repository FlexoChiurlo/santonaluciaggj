﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;


public class GameManager : MonoBehaviour {

	public static GameManager instance = null;

    public List<string> answer1 = new List<string>();
    public List<string> answer2 = new List<string>();
    public List<string> answer3 = new List<string>();
    public List<string> answer4 = new List<string>();
    public List<int> rightAnswer = new List<int>();
    public List<int> dialogueToJump = new List<int>();
    public List<string> clientReaction = new List<string>();

    private DialogueFlow dialogueFlow;
    public float credibility = 100f;
	bool credibilityDiminuishingActivated = false;
	int actualQuestion = 0;
	private GameObject santa;
    private GameObject cliente;

    //public variables

    public float pointToSubstractIfWrong = 10f;
	public float pointToAddIfCorrect = 10f;
	 DialogueText dialoguetext;
	// Use this for initialization



	public List<Button> answerButtons;
	Vector3 lastMousePosition;
	private GameObject tastiera;
	private GameObject sliderCred;
	private GameObject logPanel;

	public List<Sprite> logList;
	private AudioSource audiosrc; 

	public AudioClip clienteArrabbiato;

	void Awake(){
		if(instance == null)
			instance = this;
		else if (!instance != this)
			Destroy(gameObject);

	}

	void Start () {
		audiosrc = GetComponent<AudioSource> ();
		tastiera = GameObject.Find ("Tastiera");
		logPanel = GameObject.Find ("LogPanel");

		dialogueFlow = GetComponent<DialogueFlow> ();
		dialoguetext = GetComponent<DialogueText> ();
		sliderCred = GameObject.Find ("SliderCredibilita");

		answer1 = dialoguetext.answer1;
		answer2 = dialoguetext.answer2;
		answer3 = dialoguetext.answer3;
		answer4 = dialoguetext.answer4;
        clientReaction = dialoguetext.clientReaction;

        rightAnswer = dialoguetext.rightAnswer;
		foreach (Button b in answerButtons) {
			b.gameObject.SetActive(false);
		}

		santa = GameObject.Find ("Santa");
        cliente = GameObject.Find("Cliente");

        StartCoroutine(ChangeLog ());


	}
	
	// Update is called once per frame
	void Update () {
		if (Input.mousePosition != lastMousePosition) {
			tastiera.GetComponent<Animator> ().SetBool ("Attiva", true);
		} else {
			tastiera.GetComponent<Animator> ().SetBool ("Attiva", false);
		}
		sliderCred.GetComponent<Slider> ().value = credibility;
		lastMousePosition = Input.mousePosition;
		if (credibility <= 0f) {
			Gameover ();
		}
		if (credibility <= 20f) {
			santa.GetComponent<Animator> ().SetInteger ("Stato", 4);
		} 
	}

	IEnumerator ChangeLog(){
		while (true) {
			int randomSeconds = Random.Range (3, 6);
			yield return new WaitForSeconds (randomSeconds);
			int randomIndex = Random.Range (0, logList.Count);
			logPanel.GetComponent<Image> ().sprite = logList [randomIndex];
		}
	}

	IEnumerator DiminuishCredibilityOverTime(float seconds, float ammountToSubtract){
		yield return new WaitForSeconds (seconds);
		while(credibilityDiminuishingActivated){
			credibility -= ammountToSubtract;
			yield return new WaitForSeconds (seconds);
		}
	}

	void CorrectAnswer(){
		credibility = credibility + pointToAddIfCorrect;
		credibilityDiminuishingActivated = false;
		Debug.Log ("correct");
        dialogueFlow.Proceed();
        actualQuestion += 1;
	}

	IEnumerator WaitAndProceed(){
		santa.GetComponent<Animator> ().SetInteger ("Stato", 3);
        dialogueFlow.dialogue2.GetComponentInChildren<Text>().text = clientReaction[actualQuestion];
        dialogueFlow.dialogue2.gameObject.SetActive(true);
        dialogueFlow.dialogue1.gameObject.SetActive(false);

        cliente.GetComponent<Animator>().SetInteger("Stato", 1);


        yield return new WaitForSeconds (1.5f);
        cliente.GetComponent<Animator>().SetInteger("Stato", 0);
        dialogueFlow.dialogue2.gameObject.SetActive(false);
        dialogueFlow.dialogue1.gameObject.SetActive(true);

        dialogueFlow.Proceed (dialogueFlow.currentDialogueLine);
	}

	void WrongAnswer(){
		credibility = credibility - pointToSubstractIfWrong;
		Debug.Log ("wrong");
		//dialogueFlow.Proceed (dialogueFlow.currentDialogueLine);
		StartCoroutine(WaitAndProceed());

    }

	public void AskQuestion(){

        answerButtons[0].GetComponentInChildren<Text>().text = answer1[actualQuestion];
        answerButtons[1].GetComponentInChildren<Text>().text = answer2[actualQuestion];
        answerButtons[2].GetComponentInChildren<Text>().text = answer3[actualQuestion];
        answerButtons[3].GetComponentInChildren<Text>().text = answer4[actualQuestion];

        foreach (Button b in answerButtons) {
            b.gameObject.SetActive(true);
        }

		if (!credibilityDiminuishingActivated) {
			credibilityDiminuishingActivated = true;
			StartCoroutine(DiminuishCredibilityOverTime(1f,1f));
		}

	}

	public void AnswerQuestion(int whichResponse){
		Debug.Log (actualQuestion);
		if(whichResponse == rightAnswer[actualQuestion])
        {
            CorrectAnswer();
        } else
        {
            WrongAnswer();
			//audiosrc.clip = clienteArrabbiato;
			//audiosrc.Play ();
        }


		foreach (Button b in answerButtons) {
            b.gameObject.SetActive(false);
        }
        //StartCoroutine(WaitAndStartQuestion(5f));
    }


	public void Gameover(){
		SceneManager.LoadScene ("Game_Loss");

	}
}
