﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DialogueFlow : MonoBehaviour
{
    public List<string> dialogueLines = new List<string>();
    public List<int> dialogueSpeaker = new List<int>(); // 1 = santona, 2 = cliente, 3 = domanda
    public int currentDialogueLine = 0;
    bool goOn = false;
    bool goToNextDialog = true;
    public Button dialogue1;
    public Button dialogue2;

	private GameObject santa;
	private GameObject cliente;

    public DialogueText dialoguetext;
    void Start()
    {
		dialoguetext = GetComponent<DialogueText> ();

		dialogueLines = dialoguetext.dialogueLines;
		dialogueSpeaker = dialoguetext.dialogueSpeaker;


		santa = GameObject.Find ("Santa");
		cliente = GameObject.Find ("Cliente");
    }
		

    public void Proceed(int newCurrentDialogueLine)
    {
        currentDialogueLine = newCurrentDialogueLine;
        StartCoroutine(GoOn(0));
    }

    public void Proceed()
    {
        Proceed(currentDialogueLine + 1);
    }

    IEnumerator GoOn(int waitTime)
    {
            yield return new WaitForSeconds(waitTime);
            goToNextDialog = true;
    }

    // Update is called once per frame
    void Update()
    {

        if(currentDialogueLine >= dialogueLines.Count)
        {
			SceneManager.LoadScene ("Game_Victory");
            return;
        }
        
        if (goToNextDialog)
        {
            goToNextDialog = false;
            //goOn = true;

            if (dialogueSpeaker[currentDialogueLine] == 1)
            {
                dialogue1.GetComponentInChildren<Text>().text = dialogueLines[currentDialogueLine];
                dialogue1.gameObject.SetActive(true);
                dialogue2.gameObject.SetActive(false);
				if (GameManager.instance.credibility <= 20f) {
					santa.GetComponent<Animator> ().SetInteger ("Stato", 4);
				} else {
					santa.GetComponent<Animator> ().SetInteger ("Stato", 1);
				}

				cliente.GetComponent<Animator> ().SetInteger ("Stato", 0);

            }

            if (dialogueSpeaker[currentDialogueLine] == 2)
            {
                dialogue2.GetComponentInChildren<Text>().text = dialogueLines[currentDialogueLine];
                dialogue2.gameObject.SetActive(true);
                dialogue1.gameObject.SetActive(false);
				if (GameManager.instance.credibility <= 20f) {
					santa.GetComponent<Animator> ().SetInteger ("Stato", 4);
				} else {
					santa.GetComponent<Animator> ().SetInteger ("Stato", 0);
				}
				cliente.GetComponent<Animator> ().SetInteger ("Stato", 1);


            }

            if (dialogueSpeaker[currentDialogueLine] == 3)
            {
                dialogue1.GetComponentInChildren<Text>().text = dialogueLines[currentDialogueLine];
                dialogue1.gameObject.SetActive(true);
                dialogue2.gameObject.SetActive(false);
                GameManager.instance.AskQuestion();
				if (GameManager.instance.credibility <= 20f) {
					santa.GetComponent<Animator> ().SetInteger ("Stato", 4);
				} else {
					santa.GetComponent<Animator> ().SetInteger ("Stato", 2);
				}
				cliente.GetComponent<Animator> ().SetInteger ("Stato", 0);
            }


			//ora, nel caso NON debba fermarmi, vado all
			if (dialogueSpeaker [currentDialogueLine] == 1 || dialogueSpeaker [currentDialogueLine] == 2) {
				currentDialogueLine++;
				StartCoroutine (GoOn (2));
			}
            
        }

        /*if (goOn)
        {
            goOn = false;
			if (dialogueSpeaker [currentDialogueLine] != 3) {
				StartCoroutine (GoOn (2));
			} 
        }*/
    }
}
