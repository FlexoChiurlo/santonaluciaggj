﻿using System.Collections;
using UnityEngine;

[ExecuteInEditMode]
public class PixelDensityCamera : MonoBehaviour
{

    public float PixelsToUnits
    {
        get { return pixels_to_units_; }
        set { pixels_to_units_ = value; }
    }

    private float pixels_to_units_ = 200;
    private Camera camera_ref_;

    void Start()
    {
        camera_ref_ = GetComponent<Camera>();
    }

    void Update()
    {
       if (Screen.height > 768 )
        {
            camera_ref_.orthographicSize = Screen.height / PixelsToUnits / 3;
        }
        if (Screen.height > 1400)
        {
            camera_ref_.orthographicSize = Screen.height / PixelsToUnits / 4;
        }
        if (Screen.height > 1600)
        {
            camera_ref_.orthographicSize = Screen.height / PixelsToUnits / 5;
        }
        if (Screen.height <= 768)
        {
                  camera_ref_.orthographicSize = Screen.height / PixelsToUnits / 2;
        }
    }

    /// <summary>
    /// Smoothly zooms the camera to the requested pixel to units value and position.
    /// </summary>
    /// <param name="_new_pixels_to_units">Pixel to units value to zoom to.</param>
    /// <param name="_duration">Animation duration.</param>
    /// <param name="_camera_position">New camera position.</param>
    public void smoothZoom( float _new_pixels_to_units , float _duration , Vector3 _camera_position )
    {
		StartCoroutine( smoothZoomRoutine(_new_pixels_to_units, _duration , _camera_position ));
    }

    /// <summary>Actual zooming routine.</summary>
    private IEnumerator smoothZoomRoutine( float _new_pixel , float _duration , Vector3 _camera_position )
    {
        float start_time = Time.time;
        float alpha;
        float start_pixels = PixelsToUnits;

        while ( ( Time.time - start_time ) < _duration )
        {
			alpha = Mathf.Clamp01( ( Time.time - start_time ) / _duration );
            PixelsToUnits = Mathf.Lerp( start_pixels , _new_pixel , alpha );
			camera_ref_.transform.position = Vector3.Lerp( camera_ref_.transform.position , _camera_position , alpha );
            
			yield return null;
        }

        // Final update
        PixelsToUnits = _new_pixel;
        camera_ref_.transform.position = _camera_position;
    }

}
