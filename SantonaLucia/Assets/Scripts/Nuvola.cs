﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Nuvola : MonoBehaviour {
	public float timeCambio;
	public float speed;

	public int dir;
    Vector3 moveVector;
	// Use this for initialization
	void Start () {
		if (dir == 1) {
			moveVector = Vector3.left;
		} else {
			moveVector = Vector3.right;
		}
		StartCoroutine (ChangeDir ());
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = transform.position + new Vector3 ( moveVector.x,0, 0) * speed * Time.deltaTime;
	}


	IEnumerator ChangeDir(){
		while (true) {
			yield return new WaitForSeconds (timeCambio);
			if (moveVector == Vector3.right) {
				moveVector = Vector3.left;
			} else {
				moveVector = Vector3.right;
			}
		
		}
	
	}
}
