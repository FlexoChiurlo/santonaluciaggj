﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueText : MonoBehaviour {
    public List<string> dialogueLines = new List<string>();
    public List<int> dialogueSpeaker = new List<int>(); // 1 = santona, 2 = cliente, 3 = domanda
    public List<string> clientReaction = new List<string>();


    public List<string> answer1 = new List<string>();
    public List<string> answer2 = new List<string>();
    public List<string> answer3 = new List<string>();
    public List<string> answer4 = new List<string>();
    public List<int> rightAnswer = new List<int>();

    // Use this for initialization
    void Start () {
/*
        dialogueSpeaker.Add(1); dialogueLines.Add("");
        dialogueSpeaker.Add(2); dialogueLines.Add("");
        dialogueSpeaker.Add(3); dialogueLines.Add("");

        answer1.Add("");
        answer2.Add("");
        answer3.Add("");
        answer4.Add("");
        rightAnswer.Add();
        clientReaction.Add("");
*/

        dialogueSpeaker.Add(1); dialogueLines.Add("Welcome, welcome, my friend. Come in.");
        dialogueSpeaker.Add(1); dialogueLines.Add("The stars brought you here.");
        dialogueSpeaker.Add(1); dialogueLines.Add("Now say your name out loud, let the stars hear it.");
        dialogueSpeaker.Add(1); dialogueLines.Add("To create the astral connection.");
        dialogueSpeaker.Add(2); dialogueLines.Add("It’s Mario Merola, ma’am.");
        dialogueSpeaker.Add(1); dialogueLines.Add("Say no more. I know everything about you already. I see...");
        dialogueSpeaker.Add(3); dialogueLines.Add("You live in ... and you’re ...");

        dialogueSpeaker.Add(1); dialogueLines.Add("You live in Altamura and you’re 46.");
        dialogueSpeaker.Add(1); dialogueLines.Add("Sit down and be patient, Mario.");
        dialogueSpeaker.Add(1); dialogueLines.Add("Cartomancy is a complex art.");
        dialogueSpeaker.Add(3); dialogueLines.Add("I also see a person, someone very close...");

        dialogueSpeaker.Add(2); dialogueLines.Add("Yes, you see...");
        dialogueSpeaker.Add(2); dialogueLines.Add("Things with her are a bit up and down. She’s up to something. But what?");
        dialogueSpeaker.Add(2); dialogueLines.Add("I wanted to buy her a present to make her happy.");
        dialogueSpeaker.Add(3); dialogueLines.Add("You should buy her ...");

        dialogueSpeaker.Add(2); dialogueLines.Add("Yes, yes. A real beauty.");
        dialogueSpeaker.Add(1); dialogueLines.Add("Wait, wait.");
        dialogueSpeaker.Add(3); dialogueLines.Add("The stars are telling me that...");

        dialogueSpeaker.Add(2); dialogueLines.Add("I muert d giud!");
        dialogueSpeaker.Add(2); dialogueLines.Add("I knew it!");
        dialogueSpeaker.Add(3); dialogueLines.Add("I see...");

        dialogueSpeaker.Add(2); dialogueLines.Add("Virgin Mary! Where are those two?");
        dialogueSpeaker.Add(3); dialogueLines.Add("I can see them. They are meeting ...");


        dialogueSpeaker.Add(2); dialogueLines.Add("The stars are accurate for sure.");
        dialogueSpeaker.Add(1); dialogueLines.Add("The stars don’t lie.");
        dialogueSpeaker.Add(2); dialogueLines.Add("I wish I knew this dog’s face!");
        dialogueSpeaker.Add(2); dialogueLines.Add("Stealing my wife!");

        dialogueSpeaker.Add(3); dialogueLines.Add("I can see him. He's ...");

        dialogueSpeaker.Add(2); dialogueLines.Add("Oh my God! I knew it.");
        dialogueSpeaker.Add(2); dialogueLines.Add("Well, you are a saint indeed.");
        dialogueSpeaker.Add(2); dialogueLines.Add("Your power is beyond my expectations!");
        dialogueSpeaker.Add(2); dialogueLines.Add("Take my money, all of them.");
        dialogueSpeaker.Add(2); dialogueLines.Add("I have a situation to solve now.");
		dialogueSpeaker.Add(2); dialogueLines.Add("I have a situation to solve now.");

        answer1.Add("You live in Bari and you’re 35.");
        answer2.Add("You live in Taranto and you’re 52.");
        answer3.Add("You live in Altamura and you’re 46.");
        answer4.Add("You live in Brindisi and you’re 43.");
        rightAnswer.Add(3);
        clientReaction.Add("Well, I don’t want to waste my money here...");

        answer1.Add("Your wife Rita, what a beauty.");
        answer2.Add("Your dog Ugo, I don’t judge.");
        answer3.Add("A man in a mask, God is watching you.");
        answer4.Add("Me, I don’t blame you.");
        rightAnswer.Add(1);
        clientReaction.Add("It’s not a good moment for jokes.");

        answer1.Add("A ring.");
        answer2.Add("A bouquet of 69 red roses.");
        answer3.Add("A car, a red car.");
        answer4.Add("A trip to Monkey Island.");
        rightAnswer.Add(3);
        clientReaction.Add("Not exactly that...");

        answer1.Add("She's got an issue with drinking.");
        answer2.Add("She’s messaging another man.");
        answer3.Add("She's pregnant.");
        answer4.Add("She's possessed by the Devil.");
        rightAnswer.Add(2);
        clientReaction.Add("Not at all, that's impossible!");

        answer1.Add("You can make a penis using symbols.");
        answer2.Add("He’s a stallion.");
        answer3.Add("Your wife likes real men.");
        answer4.Add("They’re together now.");
        rightAnswer.Add(4);
        clientReaction.Add("How dare you?");


        answer1.Add("At the cinema.");
        answer2.Add("At the park.");
        answer3.Add("In his house.");
        answer4.Add("In your house.");
        rightAnswer.Add(2);
        clientReaction.Add("Are you kidding me?");

        answer1.Add("Antonio, the butcher.");
        answer2.Add("Pasquale, the fisherman.");
        answer3.Add("Salvatore, the pizza boy.");
        answer4.Add("Nicola, the baker.");
        rightAnswer.Add(1);
        clientReaction.Add("No! I don't believe you, he's a good guy!");

    }

    // Update is called once per frame
    void Update () {
		
	}
}
