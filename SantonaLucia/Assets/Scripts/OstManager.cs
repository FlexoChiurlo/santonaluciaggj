﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OstManager : MonoBehaviour {
	public AudioClip loopOst;

	private AudioSource audiosrc; 

	// Use this for initialization
	void Start () {
		audiosrc = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	IEnumerator WaitEndOfOstAndChange(){
		float lenght = audiosrc.clip.length;
		yield return new WaitForSeconds (lenght);
		audiosrc.clip = loopOst;
	}
}
