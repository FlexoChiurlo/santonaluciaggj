﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Sprite/FadeHorizontal"
{
	Properties
	{
		_FadeAmount("Fade Amount", Range(0.0, 1.0)) = 1.0
		_MainTex("Texture", 2D) = "white" {}
		_FromLeft("FromLeft", Range(0, 1)) = 1
	}
		SubShader
	{
		Tags
		{ 
			"RenderType" = "Transparent" 
			"Queue" = "Transparent" 
		}
		Blend SrcAlpha OneMinusSrcAlpha
		//ZWrite On
		LOD 100
		Cull Front
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			//#pragma fragmentoption ARB_precision_hint_fastest

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float2 uv : TEXCOORD0;
			};

			fixed _FadeAmount;
			sampler2D _MainTex;
			float4 _MainTex_ST;
			bool _FromLeft;

			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}

			fixed4 frag(v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv);
			
				if (_FromLeft)
				{
					//col.rgb = col.rgb * (i.uv.x < _FadeAmount);

					col.a = col.a * (i.uv.x < _FadeAmount);
				}
				else
				{
				    //col.rgb = col.rgb * ((1.0 - i.uv.x) < _FadeAmount);

					col.a = col.a * ((1.0 - i.uv.x) < _FadeAmount);
				}
			
			return col;
			}
		ENDCG
		}
	}
}
